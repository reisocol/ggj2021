// Copyright Epic Games, Inc. All Rights Reserved.

#include "GGJ_Montreal2021GameMode.h"
#include "GGJ_Montreal2021HUD.h"
#include "GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGGJ_Montreal2021GameMode::AGGJ_Montreal2021GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/GameCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
}
